# Flight Passengers (Client) - Spring Boot

This is a simple Client flight/passengers REST API made with Spring Boot for the following operations:

* retrieve list of flights and passengers from [main service](https://bitbucket.org/paulobing/passengers-operations) and refresh local independent database 
* search passengers by name, lastname or seat number

There is no authentication implemented.  
Below you can find some details and instructions on how to use it.  
**Depends on [main service](https://bitbucket.org/paulobing/passengers-operations), since it communicates with it**

**LIMITATIONS**  
1: service Refresh/Retrieve from Other API is **not working** (**it is in the other branch**, where you can see in this [Pull Request](https://bitbucket.org/paulobing/passengers-client-service/pull-requests/1)), I'd need more time to investigate and build more IT and Unit Testing
2: port is fixed on 8888, no configuration is possible
3: communication to Main API is made through 8080 on the same hostname (localhost)  
4: no seat number validation (it accepts any string)

**VALIDATIONS**  
1: when retrieving list from the main service, show a message if the server is down or some error took place

**NOTES**
1: no docker so far  


## Start Flight Passengers Client Spring Boot
Requirements:

* [git](https://git-scm.com/downloads)
* [maven](https://maven.apache.org/download.cgi)
* [Java 8+](https://www.java.com/download/) installed in your environment
* have mvn executable set in your PATH

Steps:

1. Clone this bitbucket repo in your local environment onto a folder of your choice executing below:  
 ```
 git clone https://paulobing@bitbucket.org/paulobing/passengers-client-service.git
 ```
2. To start springboot execute from inside the downloaded directory with the project (it might take a few minutes the first time to download dependencies):  
 ```
 mvn spring-boot:run
 ```
3. Choose one of below endpoints or swagger-ui to test the endpoints.

Tests:

There are some test cases and you may execute the tests or clean install to check full compile and tests execution (including rest-assured endpoint tests):  
 ```
 mvn clean install
 ```
 ```
 mvn test
 ```


## Swagger API Interface
To see the API in an interactive way, you can access below url, which will redirect to swagger-ui page:
```
http://localhost:8888/
```

# API Endpoints
As mentioned above, below is a list of endpoints available to perform each action with an example:

## UpdateManifest (GET) -- not working yet. NullCheck fail
To retrieve data from API 1 (Main) and update flights and passengers in this service's DB:
```java
GET localhost:8888/manifest/update/{flightNumber}
```
Example:
```
curl --request GET http://localhost:8888/manifest/update/VL1234
```

## GetPassenger (POST)
**Note**: Had to set it to POST because it receives a body. Ideally I would send the query as path variables and GET method.
To get list of passengers sending as data name or lastname or seat:
```java
POST localhost:8888/search
```
Example:
```java
curl --request POST http://localhost:8888/search -H "Content-Type: application/json" --data '{ "name": "Arnold", "lastname": "Schwarzenegger", "seat": "1A" }'
```

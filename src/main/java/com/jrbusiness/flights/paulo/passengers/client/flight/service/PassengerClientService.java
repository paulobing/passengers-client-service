package com.jrbusiness.flights.paulo.passengers.client.flight.service;

import java.io.IOException;

import com.jrbusiness.flights.paulo.passengers.client.flight.dto.PassengerMainApiDTO;
import com.jrbusiness.flights.paulo.passengers.client.flight.dto.PassengerSearchResultsDTO;

public interface PassengerClientService {
    void refreshFlightPassengers(String flightNumber) throws IOException;
    PassengerSearchResultsDTO findPassengers(PassengerMainApiDTO passengerSearchDTO);
}

package com.jrbusiness.flights.paulo.passengers.client.flight.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jrbusiness.flights.paulo.passengers.client.flight.dto.FlightMainApiDTO;
import com.jrbusiness.flights.paulo.passengers.client.flight.dto.PassengerDTO;
import com.jrbusiness.flights.paulo.passengers.client.flight.dto.PassengerMainApiDTO;
import com.jrbusiness.flights.paulo.passengers.client.flight.dto.PassengerSearchResultsDTO;
import com.jrbusiness.flights.paulo.passengers.client.flight.model.Flight;
import com.jrbusiness.flights.paulo.passengers.client.flight.model.Passenger;
import com.jrbusiness.flights.paulo.passengers.client.flight.repository.FlightRepository;
import com.jrbusiness.flights.paulo.passengers.client.flight.repository.PassengerRepository;

@Service
public class PassengerClientServiceImpl implements PassengerClientService {
    private FlightRepository flightRepository;
    private PassengerRepository passengerRepository;
    private OperationApiService operationApiService;
    private ModelMapper modelMapper;

    public PassengerClientServiceImpl() {
    }
    
    @Autowired
    public PassengerClientServiceImpl(FlightRepository flightRepository, PassengerRepository passengerRepository, OperationApiService operationApiService, ModelMapper modelMapper) {
        this.flightRepository = flightRepository;
        this.passengerRepository = passengerRepository;
        this.operationApiService = operationApiService;
        this.modelMapper = modelMapper;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void refreshFlightPassengers(String flightNumber) throws IOException {
        FlightMainApiDTO flightMainApiDTO = operationApiService.retrieveFlightDataFromExternalService(flightNumber);
        
        Optional<Flight> flightLocalDatabase = flightRepository.findByFlightNumber(flightNumber);
        if (flightLocalDatabase.isPresent()) {
            flightRepository.deleteById(flightLocalDatabase.get().getId());
        }
        
        System.out.println("*** BEFORESAVE: " + flightNumber);
        flightRepository.save(convert(flightMainApiDTO));
        System.out.println("*** AFTERSAVE : " + flightNumber);
    }

    @Override
    public PassengerSearchResultsDTO findPassengers(PassengerMainApiDTO passengerSearchDTO) {
        return new PassengerSearchResultsDTO(convert(passengerRepository.findAllByNameOrLastnameOrSeat(passengerSearchDTO.getName(), passengerSearchDTO.getLastname(), passengerSearchDTO.getSeat())));
    }

    private Flight convert(FlightMainApiDTO flightMainApiDTO) {
        System.out.println("Flight Number: " + flightMainApiDTO.getFlight());
        Flight flight = modelMapper.typeMap(FlightMainApiDTO.class, Flight.class).map(flightMainApiDTO);
        List<Passenger> passengers = convertToListPassenger(flightMainApiDTO.getPassengers(), flightMainApiDTO.getFlight());
        passengers.stream().forEach(p -> p.setFlight(flight));
        flight.setPassengers(passengers);
        flight.setFlightNumber(flightMainApiDTO.getFlight());
        System.out.println("Flight: " + flight);
        return flight;
    }

    private List<Passenger> convertToListPassenger(List<PassengerMainApiDTO> passengersDTO, String flightNumber) {
        return passengersDTO.stream().map(passengerMainApiDTO -> buildPassenger(passengerMainApiDTO, flightNumber)).collect(Collectors.toList()); // manual mapping
    }
    
    // TODO this isn't the best practice. It should be refactored. No time now :)
    public Passenger buildPassenger(PassengerMainApiDTO passengerDTO, String flightNumber) {
        Passenger passenger = convert(passengerDTO);
        System.out.println("*** BEFORE getOrCreateNewFlight: " + flightNumber);
        passenger.setFlight(findOrCreateNewFlight(flightNumber));
        System.out.println("*** AFTER getOrCreateNewFlight: " + flightNumber);
        
        return passenger;
    }

    // TODO this isn't the best practice. It should be refactored. No time now :)
    private Flight findOrCreateNewFlight(String flightNumber) {
        return flightRepository.findByFlightNumber(flightNumber).orElseGet(() -> new Flight(flightNumber));
    }

    private List<PassengerDTO> convert(List<Passenger> passengers) {
        java.lang.reflect.Type targetListType = new TypeToken<List<PassengerDTO>>() {}.getType();
        return modelMapper.map(passengers, targetListType);
    }

    private Passenger convert(PassengerMainApiDTO passengerMainApiDTO) {
        return modelMapper.typeMap(PassengerMainApiDTO.class, Passenger.class).map(passengerMainApiDTO);
    }
    
}

package com.jrbusiness.flights.paulo.passengers.client.flight.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.jrbusiness.flights.paulo.passengers.client.flight.model.Passenger;

@Repository
public interface PassengerRepository extends CrudRepository<Passenger, Long> {
    List<Passenger> findAllByNameOrLastnameOrSeat(String name, String lastname, String seat);
}

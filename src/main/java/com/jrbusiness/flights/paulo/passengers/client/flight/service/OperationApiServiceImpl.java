package com.jrbusiness.flights.paulo.passengers.client.flight.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.jrbusiness.flights.paulo.passengers.client.RestTemplateResponseErrorHandler;
import com.jrbusiness.flights.paulo.passengers.client.common.exception.MainApiHttpClientException;
import com.jrbusiness.flights.paulo.passengers.client.common.exception.MainApiHttpServerException;
import com.jrbusiness.flights.paulo.passengers.client.common.exception.MainApiNotAvailableException;
import com.jrbusiness.flights.paulo.passengers.client.flight.dto.FlightMainApiDTO;

@Service
public class OperationApiServiceImpl implements OperationApiService {
    private RestTemplate restTemplate;

    /*
    @Autowired
    public OperationApiServiceImpl(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder
                .errorHandler(new RestTemplateResponseErrorHandler())
                .build();
    }
    */

    @Autowired
    public OperationApiServiceImpl() {
        restTemplate = new RestTemplate();
    }

    @Override
    public FlightMainApiDTO retrieveFlightDataFromExternalService(String flightNumber) throws IOException {
        try {
            ResponseEntity<FlightMainApiDTO> responseEntity = 
                    restTemplate.getForEntity(
                         "http://localhost:8080/manifest/{flightNumber}", FlightMainApiDTO.class, flightNumber);
            return responseEntity.getBody();
        } catch (HttpClientErrorException e) {
            throw getCorrespondingException(e);
        }
    }
    
    // FIXME have a handler do that and solve the autowired and inner class injection problem
    private IOException getCorrespondingException(HttpClientErrorException exception) {
        if (exception.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR) {
            return new MainApiHttpServerException(exception.getStatusCode().value() + " - Server error: " + exception.getStatusText());
        } else if (exception.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR) {
            if (exception.getStatusCode() == HttpStatus.NOT_FOUND) {
                return new MainApiNotAvailableException("404 - Main API Service was not found");
            } else {
                return new MainApiHttpClientException(exception.getStatusCode().value() + " - Client error: " + exception.getStatusText());
            }
        }
        return new IOException("Generic IO Error: " + exception.getMessage());
    }
    

}

package com.jrbusiness.flights.paulo.passengers.client.flight.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.jrbusiness.flights.paulo.passengers.client.flight.model.Flight;

@Repository
public interface FlightRepository extends CrudRepository<Flight, Long> {
    Optional<Flight> findByFlightNumber(String flightNumber);
}

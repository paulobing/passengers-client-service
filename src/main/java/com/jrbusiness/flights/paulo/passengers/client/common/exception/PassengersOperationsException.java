package com.jrbusiness.flights.paulo.passengers.client.common.exception;

public abstract class PassengersOperationsException extends Exception {
    public PassengersOperationsException(String message) {
        super(message);
    }
}

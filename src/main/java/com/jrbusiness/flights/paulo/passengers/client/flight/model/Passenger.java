package com.jrbusiness.flights.paulo.passengers.client.flight.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@SequenceGenerator(name="seq_passenger", initialValue=1, allocationSize=100)
@Getter
@Setter
@Table(name="cli_flight_passenger",
    uniqueConstraints={
        @UniqueConstraint(name="flight_passenger_uq_flight_seat", columnNames= {"flight_id", "seat"}),
        @UniqueConstraint(name="flight_passenger_uq_flight_person", columnNames= {"flight_id", "name", "lastname"})
    })
@NoArgsConstructor(access=AccessLevel.PUBLIC)
@AllArgsConstructor(access=AccessLevel.PUBLIC)
public class Passenger {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_passenger")
    private Long id;
    
    @Column(nullable=false)
    @NotNull
    private String name;

    @Column(nullable=false)
    @NotNull
    private String lastname;
    
    @Column(nullable=false)
    @NotNull
    private String seat;

    @ManyToOne
    @JoinColumn(name="flight_id", nullable=false)
    private Flight flight;

}

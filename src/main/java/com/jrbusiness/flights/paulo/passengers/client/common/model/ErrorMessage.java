package com.jrbusiness.flights.paulo.passengers.client.common.model;

import java.time.LocalDateTime;
import java.time.ZoneId;

import lombok.Getter;

@Getter
public class ErrorMessage {
    private String message;
    private long timestamp;
    
    public ErrorMessage(String message) {
        this.message = message;
        this.timestamp = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

}

package com.jrbusiness.flights.paulo.passengers.client;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatus.Series;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import com.jrbusiness.flights.paulo.passengers.client.common.exception.MainApiHttpClientException;
import com.jrbusiness.flights.paulo.passengers.client.common.exception.MainApiHttpServerException;
import com.jrbusiness.flights.paulo.passengers.client.common.exception.MainApiNotAvailableException;

@Deprecated
@Component
public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse httpResponse) throws IOException {

        return (httpResponse.getStatusCode().series() == Series.CLIENT_ERROR
                || httpResponse.getStatusCode().series() == Series.SERVER_ERROR);
    }

    @Override
    public void handleError(ClientHttpResponse httpResponse) throws IOException {
        if (httpResponse.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR) {
            throw new MainApiHttpServerException(httpResponse.getStatusCode().value() + " - Server error: " + httpResponse.getStatusText());
        } else if (httpResponse.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR) {
            if (httpResponse.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new MainApiNotAvailableException("404 - Main API Service was not found");
            } else {
                throw new MainApiHttpClientException(httpResponse.getStatusCode().value() + " - Client error: " + httpResponse.getStatusText());
            }
        }
    }
}
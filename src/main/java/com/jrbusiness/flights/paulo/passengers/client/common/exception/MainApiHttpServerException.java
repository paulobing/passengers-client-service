package com.jrbusiness.flights.paulo.passengers.client.common.exception;

import java.io.IOException;

public class MainApiHttpServerException extends IOException {
    public MainApiHttpServerException(String message) {
        super(message);
    }
}


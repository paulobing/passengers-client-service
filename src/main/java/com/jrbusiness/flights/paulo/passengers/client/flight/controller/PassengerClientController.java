package com.jrbusiness.flights.paulo.passengers.client.flight.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrbusiness.flights.paulo.passengers.client.flight.dto.PassengerMainApiDTO;
import com.jrbusiness.flights.paulo.passengers.client.flight.dto.PassengerSearchResultsDTO;
import com.jrbusiness.flights.paulo.passengers.client.flight.service.PassengerClientService;

@RestController
@RequestMapping("manifest")
public class PassengerClientController {
    private PassengerClientService passengerClientService;

    @Autowired
    public PassengerClientController(PassengerClientService passengerClientService) {
        this.passengerClientService = passengerClientService;
    }
    
    @PostMapping("/search")
    public ResponseEntity<PassengerSearchResultsDTO> getFlightByFlightNumber(@RequestBody PassengerMainApiDTO passengerSearchDTO) {
        return new ResponseEntity<>(passengerClientService.findPassengers(passengerSearchDTO), HttpStatus.OK);
    }

    @GetMapping("/update/{flightNumber}")
    public ResponseEntity<HttpStatus> refreshFlightsAndPassengers(@PathVariable String flightNumber) throws IOException {
        passengerClientService.refreshFlightPassengers(flightNumber);
        return new ResponseEntity<>(HttpStatus.OK, HttpStatus.OK);
    }

}

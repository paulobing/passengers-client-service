package com.jrbusiness.flights.paulo.passengers.client.common.exception;

import java.io.IOException;

public class MainApiNotAvailableException extends IOException {
    public MainApiNotAvailableException(String message) {
        super(message);
    }
}


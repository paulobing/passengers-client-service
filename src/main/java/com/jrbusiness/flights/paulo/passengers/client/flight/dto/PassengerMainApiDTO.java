package com.jrbusiness.flights.paulo.passengers.client.flight.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor(access=AccessLevel.PUBLIC)
@NoArgsConstructor(access=AccessLevel.PUBLIC)
@ToString
public class PassengerMainApiDTO {
    private String name;
    private String lastname;
    private String seat;
}

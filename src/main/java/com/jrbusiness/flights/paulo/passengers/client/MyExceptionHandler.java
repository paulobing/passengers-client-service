package com.jrbusiness.flights.paulo.passengers.client;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.jrbusiness.flights.paulo.passengers.client.common.exception.MainApiHttpClientException;
import com.jrbusiness.flights.paulo.passengers.client.common.exception.PassengersOperationsException;
import com.jrbusiness.flights.paulo.passengers.client.common.model.ErrorMessage;

@ControllerAdvice
public class MyExceptionHandler extends ResponseEntityExceptionHandler {
    private static Logger LOGGER = LoggerFactory.getLogger(MyExceptionHandler.class);

    @ExceptionHandler(value = IOException.class)
    public ResponseEntity<ErrorMessage> handleException(IOException e) {
        ErrorMessage errorMessage = new ErrorMessage(e.getMessage());
        return new ResponseEntity<ErrorMessage>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    /*
    @ExceptionHandler(value = MainApiHttpClientException.class)
    public ResponseEntity<ErrorMessage> handleException(MainApiHttpClientException e) {
        ErrorMessage errorMessage = new ErrorMessage(e.getMessage());
        return new ResponseEntity<ErrorMessage>(errorMessage, HttpStatus.BAD_REQUEST);
    }
    */

    @ExceptionHandler(value = PassengersOperationsException.class)
    public ResponseEntity<ErrorMessage> handleException(PassengersOperationsException e) {
        ErrorMessage errorMessage = new ErrorMessage(e.getMessage());
        return new ResponseEntity<ErrorMessage>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = TransactionSystemException.class)
    public ResponseEntity<ErrorMessage> handleException(Exception ex) {
        LOGGER.error("TransactionSystemException Caught {}", ex.getMessage());
        Throwable cause = ((TransactionSystemException)ex).getRootCause();
        if (cause instanceof ConstraintViolationException) {
            LOGGER.error("ConstraintViolationException Caught {}", cause.getMessage());
        }

        Set<ConstraintViolation<?>> constraintViolations = ((ConstraintViolationException) cause)
                .getConstraintViolations();
        String msg = constraintViolations
                .stream()
                .map(constraintViolation -> constraintViolation.getMessage())
                .collect(Collectors.joining(";"));
        ErrorMessage errorMessage = new ErrorMessage(msg);

        return new ResponseEntity<ErrorMessage>(errorMessage, HttpStatus.BAD_REQUEST);
    }
    
}
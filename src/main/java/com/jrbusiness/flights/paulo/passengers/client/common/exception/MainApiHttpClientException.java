package com.jrbusiness.flights.paulo.passengers.client.common.exception;

import java.io.IOException;

public class MainApiHttpClientException extends IOException {
    public MainApiHttpClientException(String message) {
        super(message);
    }
}


package com.jrbusiness.flights.paulo.passengers.client.flight.service;

import java.io.IOException;

import com.jrbusiness.flights.paulo.passengers.client.flight.dto.FlightMainApiDTO;

public interface OperationApiService {
    FlightMainApiDTO retrieveFlightDataFromExternalService(String flightNumber) throws IOException;
}

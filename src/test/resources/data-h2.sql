insert into cli_flight (id, flight_number)
values (seq_flight.nextval, 'VL1111');
insert into cli_flight (id, flight_number)
values (seq_flight.nextval, 'VL1234');

insert into cli_flight_passenger (id, name, lastname, seat, flight_id)
select seq_passenger.nextval, 'Gustavo', 'Bing', '7A', id from cli_flight where flight_number = 'VL1111';
insert into cli_flight_passenger (id, name, lastname, seat, flight_id)
select seq_passenger.nextval, 'Camila', 'Bing', '7B', id from cli_flight where flight_number = 'VL1111';

insert into cli_flight_passenger (id, name, lastname, seat, flight_id)
select seq_passenger.nextval, 'John', 'Doe', '1A', id from cli_flight where flight_number = 'VL9876';

package com.jrbusiness.flights.paulo.passengers.client.flight.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.jrbusiness.flights.paulo.passengers.client.RestTemplateResponseErrorHandler;
import com.jrbusiness.flights.paulo.passengers.client.flight.dto.FlightMainApiDTO;
import com.jrbusiness.flights.paulo.passengers.client.flight.dto.PassengerMainApiDTO;
import com.jrbusiness.flights.paulo.passengers.client.flight.repository.FlightRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PassengerClientServiceImplTest {
    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {
//        @Autowired
//        private RestTemplateBuilder restTemplateBuilder;
        
        @Bean
        public OperationApiService operationApiService() {
            return new OperationApiServiceImpl();
        }
        
    }

//    @Mock
//    private RestTemplateBuilder restTemplateBuilder;

    //@MockBean
    private PassengerClientService passengerClientService;

    @Autowired
    //@MockBean
    private OperationApiService operationApiService;

    @Autowired
    private FlightRepository flightRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Before
    public void setUp() {
        passengerClientService = new PassengerClientServiceImpl(flightRepository, null, operationApiService, modelMapper);
        
        //Mockito.when(operationApiService.retrieveFlightDataFromExternalService(Mockito.anyString())).then(i -> buildFlightDTO(i));
        
//        operationApiService = new OperationApiServiceImpl(restTemplateBuilder);
//        Mockito.when(restTemplateBuilder.errorHandler(Mockito.any(RestTemplateResponseErrorHandler.class))).thenReturn(restTemplateBuilder);
//        Mockito.when(restTemplateBuilder.build()).thenReturn(new RestTemplate());
    }
    
    private FlightMainApiDTO buildFlightDTO(InvocationOnMock i) {
        String flightNumber = i.getArgument(0);
        List<PassengerMainApiDTO> passengers = Arrays.asList(new PassengerMainApiDTO("Paulo", "Bing", "1A"));
        return new FlightMainApiDTO(flightNumber, passengers);
    }

    @Test
    public void whenRefreshFlightPassengers_thenFlightNumberShouldBeNotNull() throws IOException {
        final String flightNumber = "FR7777";
        passengerClientService.refreshFlightPassengers(flightNumber);
    }

    @Test
    public void whenRefreshFlightTwice_thenFail() throws IOException {
        final String flightNumber = "FR1234";
        passengerClientService.refreshFlightPassengers(flightNumber);
        passengerClientService.refreshFlightPassengers(flightNumber);
    }

}

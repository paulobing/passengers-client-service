package com.jrbusiness.flights.paulo.passengers.client.flight.repository;

import java.util.Optional;


import static org.assertj.core.api.Assertions.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.jrbusiness.flights.paulo.passengers.client.flight.model.Flight;

@RunWith(SpringRunner.class)
@DataJpaTest
public class FlightRepositoryTest {

    @Autowired
    private FlightRepository flightRepository;

    @Test
    public void whenFindByFlightNumber_thenReturnFlight() {
        final String flightNumber = "VL8888";
        
        // given
        Flight flight = new Flight(flightNumber);
        flightRepository.save(flight);

        // when
        Optional<Flight> found = flightRepository.findByFlightNumber(flightNumber);

        // then
        assertThat(found.isPresent()).isTrue();
        Flight flightFound = found.get();
        assertThat(flightFound.getFlightNumber()).isEqualTo(flightNumber);
    }
}
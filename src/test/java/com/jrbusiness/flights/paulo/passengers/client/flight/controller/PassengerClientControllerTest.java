package com.jrbusiness.flights.paulo.passengers.client.flight.controller;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import org.hamcrest.Matchers;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.jrbusiness.flights.paulo.passengers.client.flight.dto.PassengerMainApiDTO;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.DEFINED_PORT)
@TestPropertySource(properties = "server.port=8888")
public class PassengerClientControllerTest {
    private static final String URL_POST_SEARCH_FLIGHT_AND_PASSENGERS = "/manifest/search";
    
    @BeforeClass
    public static void setup() {
        String port = System.getProperty("server.port");
        if (port == null) {
            RestAssured.port = Integer.valueOf(8888);
        } else {
            RestAssured.port = Integer.valueOf(port);
        }
    }
        
    @Test
    public void whenSearchExistingPassengerByName_thenPassengerMatches() {
        Response responseSearchPassengers = getPostSearchPassengersResponse(buildPassengerSearchDTO("Gustavo", null, null));
        responseSearchPassengers.prettyPrint();
        responseSearchPassengers
            .then()
            .assertThat()
            .statusCode(200)
            // from data-h2.sql
            .body("results[0].flightNumber", equalTo("VL1111"))
            .body("results[0].name", equalTo("Gustavo"))
            .body("results[0].lastname", equalTo("Bing"))
            .body("results[0].seat", equalTo("7A"));
    }

    @Test
    public void whenSearchExistingPassengerByLastname_thenPassengerMatches() {
        Response responseSearchPassengers = getPostSearchPassengersResponse(buildPassengerSearchDTO(null, "Bing", null));
        responseSearchPassengers.prettyPrint();
        responseSearchPassengers
            .then()
            .assertThat()
            .statusCode(200)
            // from data-h2.sql
            .body("results[0].flightNumber", equalTo("VL1111"))
            .body("results[0].name", equalTo("Gustavo"))
            .body("results[0].lastname", equalTo("Bing"))
            .body("results[0].seat", equalTo("7A"))
            .body("results[1].flightNumber", equalTo("VL1111"))
            .body("results[1].name", equalTo("Camila"))
            .body("results[1].lastname", equalTo("Bing"))
            .body("results[1].seat", equalTo("7B"));
    }
    
    @Test
    public void whenSearchExistingPassengerBySeat_thenPassengerMatches() {
        Response responseSearchPassengers = getPostSearchPassengersResponse(buildPassengerSearchDTO(null, null, "7A"));
        responseSearchPassengers.prettyPrint();
        responseSearchPassengers
            .then()
            .assertThat()
            .statusCode(200)
            // from data-h2.sql
            .body("results[0].flightNumber", equalTo("VL1111"))
            .body("results[0].name", equalTo("Gustavo"))
            .body("results[0].lastname", equalTo("Bing"))
            .body("results[0].seat", equalTo("7A"));
    }

    @Test
    public void whenSearchReturnsNoPassengers_thenResultIsEmpty() {
        Response responseSearchPassengersEmpty = getPostSearchPassengersResponse(buildPassengerSearchDTO(null, null, "99Z"));
        responseSearchPassengersEmpty.prettyPrint();
        responseSearchPassengersEmpty
            .then()
            .assertThat()
            .statusCode(200)
            .body("results", Matchers.empty());
    }

    private Response getPostSearchPassengersResponse(final PassengerMainApiDTO passengerSearchDTO) {
        return given()
            .contentType(ContentType.JSON)
            .body(passengerSearchDTO)
            .when()
            .post(URL_POST_SEARCH_FLIGHT_AND_PASSENGERS);
    }

    private PassengerMainApiDTO buildPassengerSearchDTO(String name, String lastname, String seat) {
        return new PassengerMainApiDTO(name, lastname, seat);
    }

    /** TODO ignore commented-out code below.
     * this would be an idea of an integration test, which would require the other service to be up **/
    /*
    private static final String URL_GET_UPDATE_FLIGHT = "/manifest/update/{flightNumber}";
    private static final String URL_EXTERNAL_POST_CREATE_FLIGHT = "http://localhost:8080/manifest/createFlight/{flightNumber}";
    private static final String CREATE_FLIGHT_NUMBER = "VL9876";
    @Test
    public void whenRefreshAndSearchExistingPassengerThenRefreshWithoutPassenger_thenFlightDoesNotHavePassenger() {
        final String flightNumber = CREATE_FLIGHT_NUMBER;
        
        getExternalPostCreateFlightResponse(flightNumber).then().assertThat().statusCode(200);
        
        Response responseSearchPassengers = getPostSearchPassengersResponse(buildPassengerSearchDTO("John", null, null));
        responseSearchPassengers.prettyPrint();
        responseSearchPassengers
            .then()
            .assertThat()
            .statusCode(200)
            // from data-h2.sql
            .body("results[0].flightNumber", equalTo(flightNumber))
            .body("results[0].name", equalTo("John"))
            .body("results[0].lastname", equalTo("Doe"))
            .body("results[0].seat", equalTo("1A"));
        
        Response responseRefreshPassengers = getGetRefreshPassengersFromExternalApiResponse(flightNumber);
        responseRefreshPassengers.prettyPrint();
        responseRefreshPassengers
            .then()
            .assertThat()
            .statusCode(200);
        
        Response responseSearchPassengers2 = getPostSearchPassengersResponse(buildPassengerSearchDTO("John", null, null));
        responseSearchPassengers2.prettyPrint();
        responseSearchPassengers2
            .then()
            .assertThat()
            .statusCode(200)
            .body("results", Matchers.empty());
    }

    private Response getGetRefreshPassengersFromExternalApiResponse(final String flightNumber) {
        return given()
            .pathParam("flightNumber", flightNumber)
            .when()
            .get(URL_GET_UPDATE_FLIGHT);
    }

    private Response getExternalPostCreateFlightResponse(final String flightNumber) {
        return given()
            .pathParam("flightNumber", flightNumber)
            .when()
            .post(URL_EXTERNAL_POST_CREATE_FLIGHT);
    }

    */

}
